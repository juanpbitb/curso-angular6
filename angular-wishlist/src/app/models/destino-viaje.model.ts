import { v4 } from "uuid";

export class DestinoViaje{
	private selected: boolean;
	public id: String = v4();
	public servicios: string[];
	
	constructor(public nombre: string, public imagenUrl: string, public votes: number = 0){
		this.servicios = ['estacionamiento', 'desayuno'];
	}
	
	isSelected(): boolean{
		return this.selected;
	}
	
	setSelected(s: boolean){
		this.selected = s;
	}

	voteUp() {
        this.votes++;
	}
	
	voteDown() {
        this.votes--;
	}
	
	voteReset(){
		this.votes = 0;
	}
}
